FROM anapsix/alpine-java:8_jdk_unlimited

ENV LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8

ARG JAVA_OPTS=""

RUN mkdir /app

WORKDIR /app

ADD app.jar .

CMD ["sh", "-c", "java ${JAVA_OPTS} -jar app.jar"]