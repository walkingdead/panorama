package com.panorama.common;

@FunctionalInterface
public interface VoidFunction {
    void execute();
}
