package com.panorama.utils;


import java.time.Instant;

public class CommonUtils {


    private CommonUtils() {
        throw new IllegalArgumentException("CommonUtils can't be instantiated");
    }

    public static boolean isTimeBeforeCurrentTime(Long time, Long timeBeforeMilli) {
        return time >= timeBeforeNowInUTC(timeBeforeMilli);
    }

    public static Long timeBeforeNowInUTC(Long timeBeforeMilli) {
        return Instant.now().toEpochMilli() - timeBeforeMilli;
    }

    public static double getAverageSafe(Long sum, Long count) {
        return count == 0 ? 0 : (double) sum / count;
    }
}
