package com.panorama.props;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "config")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GeneralProperties {

    private Long statisticTime;
    private Long networkLatency;
    private Long refreshTime;
}


