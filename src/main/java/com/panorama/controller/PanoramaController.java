package com.panorama.controller;

import com.panorama.dto.PanoramasDto;
import com.panorama.props.GeneralProperties;
import com.panorama.service.PanoramaService;
import com.panorama.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Validated
public class PanoramaController {

    private final PanoramaService panoramaService;
    private final GeneralProperties generalProperties;

    @Autowired
    public PanoramaController(PanoramaService panoramaService, GeneralProperties generalProperties) {
        this.panoramaService = panoramaService;
        this.generalProperties = generalProperties;
    }

    @PostMapping(value = "/upload")
    public ResponseEntity upload(@RequestBody @Valid PanoramasDto panoramasDto) {
        boolean isTimeInSelectedRange = CommonUtils.isTimeBeforeCurrentTime(panoramasDto.getTimestamp(),
                generalProperties.getStatisticTime());
        if (!isTimeInSelectedRange) {
            return ResponseEntity.noContent().build();
        }
        panoramaService.updatePanoramasStatistic(panoramasDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping(value = "/statistic")
    public ResponseEntity statistic() {
        return ResponseEntity.ok(panoramaService.getPanoramasStatistic());
    }

}
