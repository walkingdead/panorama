package com.panorama.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PanoramasDto {

    @NotNull(message = "Count is required")
    @Min(value = 1, message = "Count should be greater than 0")
    private Long count;

    @NotNull(message = "Timestamp is required")
    @Min(value = 0, message = "Timestamp can't be negative")
    private Long timestamp;
}
