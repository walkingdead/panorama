package com.panorama.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PanoramasStatisticDto {

    private Long sum;
    private Double avg;
    private Long max;
    private Long min;
    private Long count;
}
