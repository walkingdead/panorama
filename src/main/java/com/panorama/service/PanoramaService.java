package com.panorama.service;

import com.panorama.dto.PanoramasDto;
import com.panorama.dto.PanoramasStatisticDto;

public interface PanoramaService {

    void updatePanoramasStatistic(PanoramasDto panoramasDto);

    PanoramasStatisticDto getPanoramasStatistic();
}
