package com.panorama.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.panorama.dto.PanoramasDto;
import com.panorama.dto.PanoramasStatisticDto;
import com.panorama.props.GeneralProperties;
import com.panorama.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class PanoramaServiceImpl implements PanoramaService {

    private static ConcurrentMap<Long, PanoramasDto> STATISTIC;

    private final AtomicReference<PanoramasStatisticDto> latestPanoramasStatistic;

    private final GeneralProperties generalProperties;

    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    private final ScheduledFuture<?> schedule;

    @Autowired
    public PanoramaServiceImpl(GeneralProperties generalProperties) {
        this.generalProperties = generalProperties;
        Objects.requireNonNull(generalProperties.getStatisticTime(), "Statistic time is required");
        Objects.requireNonNull(generalProperties.getNetworkLatency(), "Network Latency time is required");
        Cache<Long, PanoramasDto> cache = CacheBuilder.newBuilder()
                .concurrencyLevel(4)
                .weakKeys()
                .expireAfterWrite(generalProperties.getStatisticTime() + generalProperties.getNetworkLatency(),
                        TimeUnit.MILLISECONDS)
                .build();
        STATISTIC = cache.asMap();
        latestPanoramasStatistic = new AtomicReference<>();
        schedule = scheduler.scheduleAtFixedRate(this::updateStatistic, 0, generalProperties.getRefreshTime(), TimeUnit.MILLISECONDS);
    }

    @PreDestroy
    public void dispose() {
        if (schedule != null) {
            schedule.cancel(true);
        }
    }

    public void updatePanoramasStatistic(PanoramasDto panoramasDto) {
        Long timeBefore = CommonUtils.timeBeforeNowInUTC(generalProperties.getStatisticTime());
        if (panoramasDto.getTimestamp() >= timeBefore) {
            STATISTIC.put(panoramasDto.getTimestamp(), panoramasDto);
        }
    }

    public PanoramasStatisticDto getPanoramasStatistic() {
        return this.latestPanoramasStatistic.get();
    }

    private void updateStatistic() {

        Long sum = 0L;
        Long max = 0L;
        Long min = Long.MAX_VALUE;
        Long count = 0L;

        Long timeBefore = CommonUtils.timeBeforeNowInUTC(generalProperties.getStatisticTime());

        for (Map.Entry<Long, PanoramasDto> entry : STATISTIC.entrySet()) {
            PanoramasDto panoramas = entry.getValue();
            Long currentTimestamp = entry.getKey();
            if (currentTimestamp >= timeBefore && currentTimestamp <= panoramas.getTimestamp()) {
                sum += panoramas.getCount();
                max = Math.max(max, panoramas.getCount());
                min = Math.min(min, panoramas.getCount());
                count++;
            }
        }
        PanoramasStatisticDto panoramasStatisticDto = PanoramasStatisticDto
                .builder()
                .sum(sum)
                .max(max)
                .min(min.equals(Long.MAX_VALUE) ? 0 : min)
                .count(count)
                .avg(CommonUtils.getAverageSafe(sum, count))
                .build();
        this.latestPanoramasStatistic.compareAndSet(this.latestPanoramasStatistic.get() , panoramasStatisticDto);
    }
}
