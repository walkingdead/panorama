package com.panorama.it.controller;


import com.panorama.common.annotation.FunctionalTestRunner;
import com.panorama.dto.PanoramasDto;
import com.panorama.dto.PanoramasStatisticDto;
import com.panorama.utils.CommonUtils;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class PanoramaControllerTestIT {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${local.server.port}")
    private String port;

    @Autowired
    private Executor executor;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void uploadPanoramaCreatedResponse() {

        HttpEntity<PanoramasDto> entity = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(0L)).count(10L).build());

        ResponseEntity uploadResponse = sendUploadRequest(entity);

        assertThat("Upload panorama status", uploadResponse.getStatusCode(), is(HttpStatus.CREATED));
    }

    @Test
    public void uploadPanoramaNoContentResponse() {

        HttpEntity<PanoramasDto> entity = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(5000L)).count(10L).build());

        ResponseEntity uploadResponse = sendUploadRequest(entity);

        assertThat("Upload panorama status", uploadResponse.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }

    @Test
    public void uploadPanoramaInvalidCountParam() {

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        HttpEntity<PanoramasDto> entity = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(5000L)).count(-20L).build());

        sendUploadRequest(entity);
    }

    @Test
    public void uploadPanoramaMissedTimeStampParam() {

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        HttpEntity<PanoramasDto> entity = new HttpEntity<>(PanoramasDto.builder().count(-20L).build());

        sendUploadRequest(entity);
    }

    @Test
    public void getStaticSequenceTest() throws InterruptedException {

        waitCacheCleanTime();

        HttpEntity<PanoramasDto> panoramasDtoFirst = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(0L)).count(10L).build());


        HttpEntity<PanoramasDto> panoramasDtoSecond = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(0L)).count(17L).build());

        ResponseEntity firstResponse = sendUploadRequest(panoramasDtoFirst);
        assertThat("First panorama status", firstResponse.getStatusCode(), is(HttpStatus.CREATED));
        ResponseEntity secondResponse = sendUploadRequest(panoramasDtoSecond);
        assertThat("Second panorama status", secondResponse.getStatusCode(), is(HttpStatus.CREATED));

        waitUpdateExecutor();

        ResponseEntity<PanoramasStatisticDto> panoramasStatisticDtoResponseEntity = sendStatisticRequest(PanoramasStatisticDto.class);

        PanoramasStatisticDto panoramasStatistic = panoramasStatisticDtoResponseEntity.getBody();

        assertThat("Panorama statistic is not null", panoramasStatistic, notNullValue());

        Assert.assertThat("Max panoramas", panoramasStatistic.getMax(), Is.is(17L));
        Assert.assertThat("Min panoramas", panoramasStatistic.getMin(), Is.is(10L));
        Assert.assertThat("Avg panoramas", panoramasStatistic.getAvg(), Is.is(13.5));
        Assert.assertThat("Count panoramas", panoramasStatistic.getCount(), Is.is(2L));
        Assert.assertThat("Sum panoramas", panoramasStatistic.getSum(), Is.is(27L));
    }

    @Test
    public void getStaticSequenceOnePanoramaIsTooLateTest() throws Exception {

        waitCacheCleanTime();

        HttpEntity<PanoramasDto> panoramasDtoFirst = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(0L)).count(10L).build());


        HttpEntity<PanoramasDto> panoramasDtoSecond = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(10_000L)).count(15L).build());

        ResponseEntity firstResponse = sendUploadRequest(panoramasDtoFirst);
        assertThat("First panorama status", firstResponse.getStatusCode(), is(HttpStatus.CREATED));

        ResponseEntity secondResponse = sendUploadRequest(panoramasDtoSecond);
        assertThat("Second panorama status", secondResponse.getStatusCode(), is(HttpStatus.NO_CONTENT));

        waitUpdateExecutor();

        ResponseEntity<PanoramasStatisticDto> panoramasStatisticDtoResponseEntity = sendStatisticRequest(PanoramasStatisticDto.class);

        PanoramasStatisticDto panoramasStatistic = panoramasStatisticDtoResponseEntity.getBody();

        assertThat("Panorama statistic is not null", panoramasStatistic, notNullValue());

        Assert.assertThat("Max panoramas", panoramasStatistic.getMax(), Is.is(10L));
        Assert.assertThat("Min panoramas", panoramasStatistic.getMin(), Is.is(10L));
        Assert.assertThat("Avg panoramas", panoramasStatistic.getAvg(), Is.is(10.0));
        Assert.assertThat("Count panoramas", panoramasStatistic.getCount(), Is.is(1L));
        Assert.assertThat("Sum panoramas", panoramasStatistic.getSum(), Is.is(10L));
    }

    @Test
    public void getStaticSequenceBothPanoramaAreTooLateTest() {

        HttpEntity<PanoramasDto> panoramasDtoFirst = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(5_000L)).count(10L).build());


        HttpEntity<PanoramasDto> panoramasDtoSecond = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(10_000L)).count(15L).build());

        ResponseEntity firstResponse = sendUploadRequest(panoramasDtoFirst);
        assertThat("First panorama status", firstResponse.getStatusCode(), is(HttpStatus.NO_CONTENT));
        ResponseEntity secondResponse = sendUploadRequest(panoramasDtoSecond);
        assertThat("Second panorama status", secondResponse.getStatusCode(), is(HttpStatus.NO_CONTENT));
        ResponseEntity<PanoramasStatisticDto> panoramasStatisticDtoResponseEntity = sendStatisticRequest(PanoramasStatisticDto.class);

        PanoramasStatisticDto panoramasStatistic = panoramasStatisticDtoResponseEntity.getBody();

        assertThat("Panorama statistic is not null", panoramasStatistic, notNullValue());
    }

    @Test
    public void getStatisticMultithreadedTest() throws Exception {

        waitCacheCleanTime();

        CountDownLatch countDownLatch = new CountDownLatch(1);

        HttpEntity<PanoramasDto> panoramasDtoFirst = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(0L)).count(10L).build());


        HttpEntity<PanoramasDto> panoramasDtoSecond = new HttpEntity<>(PanoramasDto.builder()
                .timestamp(CommonUtils.timeBeforeNowInUTC(0L)).count(15L).build());

        executor.execute(() -> {
            ResponseEntity firstResponse = sendUploadRequest(panoramasDtoFirst);
            assertThat("First panorama status", firstResponse.getStatusCode(), is(HttpStatus.CREATED));
            countDownLatch.countDown();
        });

        executor.execute(() -> {
            ResponseEntity secondResponse = sendUploadRequest(panoramasDtoSecond);
            assertThat("Second panorama status", secondResponse.getStatusCode(), is(HttpStatus.CREATED));
        });

        countDownLatch.await(300, TimeUnit.MILLISECONDS);

        waitUpdateExecutor();

        ResponseEntity<PanoramasStatisticDto> panoramasStatisticDtoResponseEntity = sendStatisticRequest(PanoramasStatisticDto.class);

        PanoramasStatisticDto panoramasStatistic = panoramasStatisticDtoResponseEntity.getBody();

        assertThat("Panorama statistic is not null", panoramasStatistic, notNullValue());

        Assert.assertThat("Max panoramas", panoramasStatistic.getMax(), is(either(is(10L)).or(is(15L))));
        Assert.assertThat("Min panoramas", panoramasStatistic.getMin(), is(either(is(10L)).or(is(15L))));
        Assert.assertThat("Avg panoramas", panoramasStatistic.getAvg(), is(either(is(10.0)).or(is(15.0)).or(is(12.5))));
        Assert.assertThat("Count panoramas", panoramasStatistic.getCount(), is(either(is(1L)).or(is(2L))));
        Assert.assertThat("Sum panoramas", panoramasStatistic.getSum(), is(either(is(10L)).or(is(15L)).or(is(25L))));
    }

    private <T> ResponseEntity<T> sendUploadRequest(HttpEntity httpEntity) {
        return restTemplate.exchange(
                "http://localhost:{port}/upload",
                HttpMethod.POST, httpEntity, new ParameterizedTypeReference<T>() {
                }, port);
    }

    private <T> ResponseEntity<T> sendStatisticRequest(Class<T> klazz) {
        return restTemplate.exchange(
                "http://localhost:{port}/statistic",
                HttpMethod.GET, null, klazz, port);
    }

    private void waitCacheCleanTime() throws InterruptedException {
        Thread.sleep(500);
    }

    private void waitUpdateExecutor() throws InterruptedException {
        Thread.sleep(100);
    }
}
