package com.panorama.common.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@Profile("test")
@ComponentScan(basePackages = "com.panorama")
public class IntegrationTestConfiguration {
}
