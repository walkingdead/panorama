package com.panorama.unit.service;

import com.panorama.common.annotation.UnitTestRunner;
import com.panorama.dto.PanoramasDto;
import com.panorama.dto.PanoramasStatisticDto;
import com.panorama.props.GeneralProperties;
import com.panorama.service.PanoramaService;
import com.panorama.service.PanoramaServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.panorama.utils.CommonUtils.timeBeforeNowInUTC;
import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


@UnitTestRunner
public class PanoramaServiceTest {


    private PanoramaService panoramaService;

    private ExecutorService executorService;

    @Before
    public void init() {
        panoramaService = new PanoramaServiceImpl(GeneralProperties
                .builder()
                .statisticTime(10_000L)
                .networkLatency(0L)
                .refreshTime(50L).build());
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    @Test
    public void updateStatisticTest() throws InterruptedException {
        panoramaService.updatePanoramasStatistic(PanoramasDto.builder().timestamp(timeBeforeNowInUTC(0L)).count(10L).build());
        panoramaService.updatePanoramasStatistic(PanoramasDto.builder().timestamp(timeBeforeNowInUTC(100L)).count(5L).build());

        waitExecutorTime();
        PanoramasStatisticDto panoramasStatistic = panoramaService.getPanoramasStatistic();

        assertThat("Max panoramas", panoramasStatistic.getMax(), is(10L));
        assertThat("Min panoramas", panoramasStatistic.getMin(), is(5L));
        assertThat("Avg panoramas", panoramasStatistic.getAvg(), is(7.5));
        assertThat("Count panoramas", panoramasStatistic.getCount(), is(2L));
        assertThat("Sum panoramas", panoramasStatistic.getSum(), is(15L));
    }

    @Test
    public void updateStatisticTestTooLateTimeStamp() throws InterruptedException {
        panoramaService.updatePanoramasStatistic(PanoramasDto.builder()
                .timestamp(timeBeforeNowInUTC(20_000L)).count(10L).build());
        panoramaService.updatePanoramasStatistic(PanoramasDto.builder()
                .timestamp(timeBeforeNowInUTC(30_000L)).count(5L).build());
        waitExecutorTime();
        PanoramasStatisticDto panoramasStatistic = panoramaService.getPanoramasStatistic();

        assertThat("Max panoramas", panoramasStatistic.getMax(), is(0L));
        assertThat("Min panoramas", panoramasStatistic.getMin(), is(0L));
        assertThat("Avg panoramas", panoramasStatistic.getAvg(), is(0.0));
        assertThat("Count panoramas", panoramasStatistic.getCount(), is(0L));
        assertThat("Sum panoramas", panoramasStatistic.getSum(), is(0L));
    }

    @Test
    public void updateStatisticTestSomeTimeStampsAreTooLate() throws InterruptedException {
        panoramaService.updatePanoramasStatistic(PanoramasDto.builder()
                .timestamp(timeBeforeNowInUTC(20_000L)).count(10L).build());
        panoramaService.updatePanoramasStatistic(PanoramasDto.builder()
                .timestamp(timeBeforeNowInUTC(5_000L)).count(8L).build());
        panoramaService.updatePanoramasStatistic(PanoramasDto.builder()
                .timestamp(timeBeforeNowInUTC(8_000L)).count(4L).build());

        waitExecutorTime();

        PanoramasStatisticDto panoramasStatistic = panoramaService.getPanoramasStatistic();

        assertThat("Max panoramas", panoramasStatistic.getMax(), is(8L));
        assertThat("Min panoramas", panoramasStatistic.getMin(), is(4L));
        assertThat("Avg panoramas", panoramasStatistic.getAvg(), is(6.0));
        assertThat("Count panoramas", panoramasStatistic.getCount(), is(2L));
        assertThat("Sum panoramas", panoramasStatistic.getSum(), is(12L));
    }

    @Test
    public void updateGetStatisticTestMultithreaded() throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(2);
        executorService.execute(() -> {
            panoramaService.updatePanoramasStatistic(PanoramasDto.builder()
                    .timestamp(timeBeforeNowInUTC(5_000L)).count(8L).build());
            countDownLatch.countDown();
        });

        executorService.execute(() -> {
            panoramaService.updatePanoramasStatistic(PanoramasDto.builder()
                    .timestamp(timeBeforeNowInUTC(4_000L)).count(13L).build());
            countDownLatch.countDown();
        });

        countDownLatch.await(200, TimeUnit.MILLISECONDS);
        waitExecutorTime();

        Future<PanoramasStatisticDto> statistic = executorService.submit(() ->
                panoramaService.getPanoramasStatistic());

        PanoramasStatisticDto panoramasStatistic = statistic.get();

        executorService.shutdown();

        assertThat("Max panoramas", panoramasStatistic.getMax(), is(13L));
        assertThat("Min panoramas", panoramasStatistic.getMin(), is(8L));
        assertThat("Avg panoramas", panoramasStatistic.getAvg(), is(10.5));
        assertThat("Count panoramas", panoramasStatistic.getCount(), is(2L));
        assertThat("Sum panoramas", panoramasStatistic.getSum(), is(21L));
    }

    @Test
    public void updateGetStatisticTestMultithreadedInTheMiddle() throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        executorService.execute(() -> {
            panoramaService.updatePanoramasStatistic(PanoramasDto.builder()
                    .timestamp(timeBeforeNowInUTC(5_000L)).count(12L).build());
            countDownLatch.countDown();
        });

        executorService.execute(() -> panoramaService.updatePanoramasStatistic(PanoramasDto.builder()
                .timestamp(timeBeforeNowInUTC(4_000L)).count(18L).build()));

        countDownLatch.await(100, TimeUnit.MILLISECONDS);
        waitExecutorTime();

        Future<PanoramasStatisticDto> statistic = executorService.submit(() ->
                panoramaService.getPanoramasStatistic());

        PanoramasStatisticDto panoramasStatistic = statistic.get();

        executorService.shutdown();

        assertThat("Max panoramas", panoramasStatistic.getMax(), is(either(is(12L)).or(is(18L))));
        assertThat("Min panoramas", panoramasStatistic.getMin(), is(either(is(12L)).or(is(18L))));
        assertThat("Avg panoramas", panoramasStatistic.getAvg(), is(either(is(12.0)).or(is(18.0)).or(is(15.0))));
        assertThat("Count panoramas", panoramasStatistic.getCount(), is(either(is(1L)).or(is(2L))));
        assertThat("Sum panoramas", panoramasStatistic.getSum(), is(either(is(12L)).or(is(30L))));
    }

    private void waitExecutorTime() throws InterruptedException {
        Thread.sleep(100L);
    }
}
