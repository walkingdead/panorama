package com.panorama.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.panorama.common.annotation.UnitTestRunner;
import com.panorama.controller.PanoramaController;
import com.panorama.dto.PanoramasDto;
import com.panorama.dto.PanoramasStatisticDto;
import com.panorama.props.GeneralProperties;
import com.panorama.service.PanoramaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(PanoramaController.class)
@UnitTestRunner
public class PanoramaControllerTest {

    @MockBean
    private PanoramaService panoramaService;

    @MockBean
    private GeneralProperties generalProperties;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void uploadPanoramaCreatedResponse() throws Exception {
        when(generalProperties.getStatisticTime()).thenReturn(Long.MAX_VALUE);
        PanoramasDto panorama = PanoramasDto.builder()
                .timestamp(System.currentTimeMillis())
                .count(10L)
                .build();
        this.mockMvc.perform(post("/upload")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(panorama)))
                .andExpect(status().isCreated());
    }

    @Test
    public void uploadPanoramaNoContentResponse() throws Exception {
        when(generalProperties.getStatisticTime()).thenReturn(1L);
        PanoramasDto panorama = PanoramasDto.builder()
                .timestamp(0L)
                .count(10L)
                .build();
        this.mockMvc.perform(post("/upload")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(panorama)))
                .andExpect(status().isNoContent());
    }

    @Test
    public void getPanoramaStatistic() throws Exception {
        when(panoramaService.getPanoramasStatistic()).thenReturn(PanoramasStatisticDto
                .builder()
                .min(0L)
                .max(2L)
                .avg(1.0)
                .sum(3L)
                .count(2L)
                .build());
        this.mockMvc.perform(get("/statistic"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"sum\":3,\"avg\":1.0,\"max\":2,\"min\":0,\"count\":2}"));
    }

    @Test
    public void uploadPanoramaCountMissing() throws Exception {
        PanoramasDto panorama = PanoramasDto.builder()
                .timestamp(0L).build();
        executeInvalidPanoramaUpload(panorama, "{\"message\":\"Parameter panoramasDto [Count is required]\"}");
    }

    @Test
    public void uploadPanoramaNotValidCount() throws Exception {
        PanoramasDto panorama = PanoramasDto.builder()
                .timestamp(0L).count(-1L).build();
        executeInvalidPanoramaUpload(panorama, "{\"message\":\"Parameter panoramasDto [Count should be greater than 0]\"}");
    }

    @Test
    public void uploadPanoramaTimeStampMissing() throws Exception {
        PanoramasDto panorama = PanoramasDto.builder()
                .count(2L).build();
        executeInvalidPanoramaUpload(panorama, "{\"message\":\"Parameter panoramasDto [Timestamp is required]\"}");
    }

    @Test
    public void uploadPanoramaTimeStampInvalid() throws Exception {
        PanoramasDto panorama = PanoramasDto.builder()
                .count(2L).timestamp(Long.MIN_VALUE).build();
        executeInvalidPanoramaUpload(panorama, "{\"message\":\"Parameter panoramasDto [Timestamp can't be negative]\"}");
    }

    private void executeInvalidPanoramaUpload(PanoramasDto panorama, String message) throws Exception {
        this.mockMvc.perform(post("/upload")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(panorama)))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(message));
    }
}
