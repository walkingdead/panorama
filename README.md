How to start application.

Basically you have two options 

 1) Open your project inside IDE and launch com.panorama.PanoramaApplication and specify VM options
    parameter `-Dspring.profiles.active=prod`

 2) - execute command ./gradlew build
    - launch application with following command
    `java -Dspring.profiles.active=prod -jar build/libs/panorama-1.0-SNAPSHOT.jar`
    
To run unit and integration run command
    `./gradlew test`
    


 You can run application from docker container:
 
 Docker command:
 
    `docker run -it -e SPRING_PROFILES_ACTIVE=prod -e SERVER_PORT=8090 -p 8090:8090 kshevchuk/panorama`
   

API endpoints:

    /upload
        POST
    
    Payload:
        {"timestamp": 1501197418798 , "count": 10}
    
    Headers:
        Content-Type: application/json
    
    Response: 
        201/204
    ------------------------------------------------------------
    
    /statistic
        GET
    
    Response:
        {
            "sum": 10,
            "avg": 5.5,
            "max": 8,
            "min": 5,
            "count: 12
        }
    
    
Explanation to configuration parameters:

  statisticTime: period of time for which user want to see statistic
  networkLatency: time diff between user send request and server handled it
  refreshTime: internal executor refresh time to recalculate statistic
    

Short application description:
 
 As mentioned in the task application will find statistic for the last X seconds.
To achieve O(1) time complexity additional Scheduled executor service
was introduced. Every X second it will recalculate data for the last Y seconds.
This executor could be a real bottleneck in the future if number of requests is too high
and statistic period high as well. Even with executor user will see up to 1 second
incorrect data.